var venueID;
function select(id) {
    console.log(id);
    venueID = id;
}
Template.chooseVenue.helpers({
    getStudentsAndModules: function () {
        var modules = Session.get("moduleIDs").split(",");
        // var modules = Template.currentData().modules.split(",");
        for (var i = 0; i < modules.length; i++) {
            modules[i] = new Meteor.Collection.ObjectID(modules[i].substring(10, modules[i].length - 2));
        }

        var moduleCodes = [];
        for (var i = 0; i < modules.length; i++) {
            moduleCodes.push(ModuleList.findOne(modules[i], {'modulesCode': 1, '_id': 0})['modulesCode']);
        }
        Session.set("modules", JSON.stringify(moduleCodes));
        var students = [];
        for (var i = 0; i < moduleCodes.length; i++) {
            students.push(StudentList.find({"modules.modulesCode": moduleCodes[i]}).fetch());
        }
        Session.set("students", JSON.stringify(students));
    },
    getVenue: function () {
        return VenueList.find({});
    },

});
Template.chooseVenue.rendered = function () {
    var selectedVenue = Session.get("venueID");
    if (selectedVenue) {
        $("#" + selectedVenue).addClass("btn-info");
    }
};

Template.chooseVenue.events({
    'click #previousButton': function () {
        Router.go('/newPlan');
    },
    'click #nextButton': function () {
        var selectedVenue = Session.get("venueID");
        if (selectedVenue) {
            Router.go('/newPlan/chooseVenue/plan');
        }
        else {
            alert("You need to select a venue to proceed!");
        }
    }

});


Template.number.helpers({
    numOfStudents: function () {
        var students = Session.get("students");
        students = JSON.parse(students);
        var totalLength = 0;
        for (var i = 0; i < students.length; i++) {
            totalLength += students[i].length;
        }
        return totalLength;
    },
});

Template.venueButton.helpers({
    hasEnoughCapacity: function (capacity) {
        var numOfStudents = function () {
            var students = Session.get("students");
            students = JSON.parse(students);
            var totalLength = 0;
            for (var i = 0; i < students.length; i++) {
                totalLength += students[i].length;
            }
            return totalLength;
        }

        return numOfStudents() <= capacity;
    },

});

Template.venueButton.events({
    "click .buttonWrap": function (e) {
        var target = e.target;
        var id = target.id;
        var btns = $('.btn-info');
        btns.removeClass("btn-info");
        $("#" + id).addClass("btn-info");
        Session.set("venueID", id);
        console.log(id);
    }
});