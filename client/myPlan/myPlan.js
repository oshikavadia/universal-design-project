/**
 * Created by Oshi on 11/30/2016.
 */
Template.myPlan.helpers({
    getSavedPlans: function () {
        return SavedPlans.find();
    },
});

Template.myPlan.events({
    'change .inputfile': function (e) {
        var label = e.currentTarget.nextElementSibling;
        var labelVal = label.innerHTML;

        var inputs = document.querySelectorAll('.inputfile');
        var fileName = '';
        if (e.currentTarget.files && e.currentTarget.files.length > 1) {
            fileName = ( e.currentTarget.getAttribute('data-multiple-caption') || '' ).replace('{count}', e.currentTarget.files.length);
        }
        else {
            fileName = e.target.value.split('\\').pop();
        }

        if (fileName)
            label.querySelector('span').innerHTML = fileName;
        else
            label.innerHTML = labelVal;
    },


    'submit form': function (e) {
        e.preventDefault();
        var file = document.querySelector('input[type=file]').files[0];
        var output;
        loadFile(file);
        output = Session.get("load");
        if (output) {
            try {
                console.log(output);
                output = JSON.parse(output);
                var result = SavedPlans.insert(output);
                if (result) {
                    alert("Plan uploaded successfully!")
                    ;
                }
                else {
                    alert("Plan already exists");
                }
            } catch (err) {
                console.log(err);
                alert("Not a valid Save file!");
            }
        }
        function loadFile(file) {
            var reader = new FileReader();

            reader.addEventListener("loadend", function () {
                // console.log(reader.result);
                Session.set("load", reader.result);
            }, false);

            if (file) {
                reader.readAsText(file);
            }
            else {
                alert("Not a valid file!");
            }
        }

    }
});

Template.savedPlan.events({

    "click .buttonSave": function (e) {
        var target = e.target;
        var id = target.id;
        var temp = SavedPlans.findOne({"_id": id});
        Session.set("day", temp.day);
        Session.set("hour", temp.hour);
        Session.set("minute", temp.minute);
        Session.set("moduleIDs", temp.moduleId);
        Session.set("month", temp.month);
        Session.set("year", temp.year);
        Session.set("venueID", temp.venueId);
        Session.set("students", JSON.stringify(temp.students));
        var modules = Session.get("moduleIDs").split(",");
        // var modules = Template.currentData().modules.split(",");
        for (var i = 0; i < modules.length; i++) {
            modules[i] = new Meteor.Collection.ObjectID(modules[i].substring(10, modules[i].length - 2));
        }

        var moduleCodes = [];
        for (var i = 0; i < modules.length; i++) {
            moduleCodes.push(ModuleList.findOne(modules[i], {'modulesCode': 1, '_id': 0})['modulesCode']);
        }
        Session.set("modules", JSON.stringify(moduleCodes));
        Router.go("/myPlan/preview");


    }
});