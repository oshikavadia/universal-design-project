Template.main.helpers({
    'isNotPlanTemplate': function () {
        var template = Router.current() && Router.current().route.getName().replace('.', '-');
        alert(!(template === "newPlan-chooseVenue.plan"));
        return !(template === "newPlan-chooseVenue.plan");
    },
});
Template.loginButtons.rendered = function () {
    Accounts._loginButtonsSession.set('dropdownVisible', false);
    // $(".login-close-text").addClass("login-front");
//     Tracker.afterFlush( function () {
//     	this.$(".accounts-dialog").css("box-shadow","none");
//     this.$(".login-close-text").hide();
// }
}