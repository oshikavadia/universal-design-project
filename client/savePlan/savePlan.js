//var venueId = Session.get('venueID').toString();
//console.log(venueId);
//var venueName = VenueList.findOne({_id: '1'}).name;
//console.log(venueName);
var venueName;
var moduleNames = [];

Template.savePlan.helpers({
    myOnLoad: function () {
        Session.set("isSaved", false);
    },
    modules: function () {
        return ModuleList.find({});
    },
    breakLine: function () {
        return "<br>";

    },
    getUserEmail : function () {
        var user = Meteor.user();
        if (user && user.emails)
            return user.emails[0].address;
    },

});

Template.savePlan.onRendered(function () {
    $('#pdfOpt').css('display','none');
    Accounts._loginButtonsSession.set('dropdownVisible', false);
    if (!this._rendered) {
        this._rendered = true;
        console.log('Template onLoad');
    }
    console.log("call set value");
    var year = Session.get("year");
    var month = Session.get("month");
    var date = Session.get("day");
    var dateString= date+ " "+month+" "+year;
    var timeString = Session.get('hour').toString() + ":" + Session.get('minute').toString();
    var venue = VenueList.find().fetch();
    var venueName = "";
    for (var i = 0; i < venue.length; i++) {
         if (venue[i]._id == Session.get("venueID")) {
                venueName = venue[i].name;
                break;
            }
    }

    $("#timeIn").val(timeString);
    $("#dateIn").val(dateString);
    
    $("#venueIn").val(venueName);
    $("#subjectIn").val(Session.get('modules'));
    $("#nSave").val((venueName + " - " + timeString + " : "+ dateString + " -  v1.1"));

});

Template.savePlan.events({
    'submit form'(event) {
        // Prevent default browser form submit
        event.preventDefault();
        var target = event.target;
        var name = target.nSave.value;
        var user = target.createdBy.value;
        Session.set("planName", name);
        Session.set("createdBy", user);
        var saveFile = {
            'createdBy': user,
            'planName': name,
            'month': Session.get("month"),
            'year': Session.get("year"),
            'day': Session.get("day"),
            'hour': Session.get("hour"),
            'minute': Session.get("minute"),
            'moduleId': Session.get("moduleIDs"),
            'venueId': Session.get("venueID"),
            'students': JSON.parse(Session.get("students"))

        };
        Session.set("saveFile", JSON.stringify(saveFile));
        SavedPlans.insert(saveFile);
        var uList = Meteor.users.find({}).fetch();
        console.log(uList);
        for (var obj in uList) {

            var temp = uList[obj].emails;
            var email = temp[0].address;
            console.log(email);
            var body = "Hi " + email + ", here is the plan for subjects " + Session.get("moduleId") + " in " + Session.get("venueId") + " at " + Session.get("hour") + ":" + Session.get("minute") + " , " + Session.get("day") + ":" + Session.get("month") + ":" + Session.get("year");
            Meteor.call('sendEmail',
                email,
                'admin@edusit.com',
                'New Plan - ' + name,
                body);
        }
        console.log("email sent")
        Session.set("isSaved", true);

    },

    'click #download': function (event) {
        event.preventDefault();
        if (!Session.get("isSaved")) {
            alert("You must save first before downloading!");
        }
        else {
            /*
             * https://stackoverflow.com/questions/36296126/server-side-route-to-download-file
             * */
            var outputFile = function (filename, data) {
                var blob = new Blob([data], {type: 'text/plain'}); // !note file type..
                if (window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(blob, filename);
                }
                else {
                    var elem = window.document.createElement('a');
                    elem.href = window.URL.createObjectURL(blob);
                    elem.download = filename;
                    document.body.appendChild(elem)
                    elem.click();
                    document.body.removeChild(elem);
                }
            }
            outputFile(Session.get("planName") + ".json", Session.get("saveFile"));
        }
    },
    'click #pdf': function () {
        $('#pdfOpt').css('display','block');
    },

    'click #invigilator':function(){
        var year = Session.get("year");
        var month = Session.get("month");
        var date = Session.get("day");
        var dateString= date+ " "+month+" "+year;
        var timeString = Session.get('hour').toString() + ":" + Session.get('minute').toString();
        var venue = VenueList.find().fetch();
        var venueName = "";
        for (var i = 0; i < venue.length; i++) {
         if (venue[i]._id == Session.get("venueID")) {
                venueName = venue[i].name;
                break;
            }
         }
        var b = [];
        b.push([{text: 'Seat Number', style: 'tableHeader'}, {
            text: ' Student Name',
            style: 'tableHeader'
        }, {text: 'Exam Subject', style: 'tableHeader'}, {
            text: 'Student Number',
            style: 'tableHeader'
        }, {text: 'Signature', style: 'tableHeader'}, {text: 'Comments', style: 'tableHeader'}]);
        var studentListComplete = JSON.parse(Session.get("studentsComplete"));
        for (var key in studentListComplete) {
            var obj = studentListComplete[key];
            var temp = [];
            temp.push(obj.seat);
            temp.push(obj.firstName + " " + obj.lastName);
            console.log(obj.seat)
            temp.push((obj.programme).courseName);
            temp.push(obj.id);
            temp.push("");
            temp.push("");
            b.push(temp);
        }
        var docDefinition = {
            content: [
                {text: 'Exam Hall: '+venueName, margin: [0, 0, 0, 10], style: 'header'},
                {text: 'Date: '+dateString, margin: [0, 0, 0, 10], style: 'header'},
                {text: 'Time: '+timeString, margin: [0, 0, 0, 10], style: 'header'},
                {
                    table: {
                        headerRows: 1,
                        body: b
                    },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 3 : 1;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 3 : 1;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                        },
                    }
                }
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 10, 0, 5]
                },
                tableExample: {
                    margin: [0, 5, 0, 5]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                }
            }
        };

        pdfMake.createPdf(docDefinition).open();
    },

    'click #students':function(){
        var year = Session.get("year");
        var month = Session.get("month");
        var date = Session.get("day");
        var dateString= date+ " "+month+" "+year;
        var timeString = Session.get('hour').toString() + ":" + Session.get('minute').toString();
        var venue = VenueList.find().fetch();
        var venueName = "";
        for (var i = 0; i < venue.length; i++) {
         if (venue[i]._id == Session.get("venueID")) {
                venueName = venue[i].name;
                break;
            }
         }
        var c = [];
        c.push([
        {text: 'Seat Number', style: 'tableHeader'}, 
        {text: 'First Name',style: 'tableHeader'}, 
        {text: 'Surname',style: 'tableHeader'},
        {text: 'Exam Subject', style: 'tableHeader'}, 
        {text: 'Student Number', style: 'tableHeader'}, 
        ]);
        var studentListComplete = JSON.parse(Session.get("studentsComplete"));
        for (var key in studentListComplete) {
            var obj = studentListComplete[key];
            var temp = [];
            temp.push(obj.seat);
            temp.push(obj.firstName);
            temp.push(obj.lastName);
            console.log(obj.programme);
            temp.push((obj.programme).courseName);
            temp.push(obj.id);
            c.push(temp);
        }
        var docDefinition = {
            content: [
                {text: 'Exam Hall: '+venueName, margin: [0, 0, 0, 10], style: 'header'},
                {text: 'Date: '+dateString, margin: [0, 0, 0, 10], style: 'header'},
                {text: 'Time: '+timeString, margin: [0, 0, 0, 10], style: 'header'},
                {
                    table: {
                        headerRows: 1,
                        body: c
                    },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 3 : 1;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 3 : 1;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                        },
                    }
                }
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 10, 0, 5]
                },
                tableExample: {
                    margin: [0, 5, 0, 5]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                }
            }
        };

        pdfMake.createPdf(docDefinition).open();

    }
});

